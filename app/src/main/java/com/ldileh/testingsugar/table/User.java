package com.ldileh.testingsugar.table;

import com.orm.SugarRecord;

/**
 * Created by ldileh on 25/10/2017.
 */

public class User extends SugarRecord{

    public String name;
    public String kode;

    public User(){}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getKode() {
        return kode;
    }

    public void setKode(String kode) {
        this.kode = kode;
    }
}
