package com.ldileh.testingsugar;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.ldileh.testingsugar.table.User;
import com.orm.SugarContext;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private ListView list;
    private Button btn_reset, btn_first, btn_change;
    private Adapter adapter;
    private List<User> item_def;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // init sugar in this activity.....
        SugarContext.init(this);

        // init view
        list = (ListView) findViewById(R.id.list);
        btn_reset = (Button) findViewById(R.id.btn_reset);
        btn_first = (Button) findViewById(R.id.btn_first);
        btn_change = (Button) findViewById(R.id.btn_change);

        List<User> users = User.listAll(User.class);
        if(users.size() < 1)
            generateRecord();

        if(users.size() > 0){
            item_def = users;
            adapter = new Adapter(users, MainActivity.this);
            list.setAdapter(adapter);
        }

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                User user = (User) adapter.getItem(position);
                user.getId();
            }
        });

        btn_reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                adapter.setmItems(item_def);
                adapter.notifyDataSetChanged();
            }
        });

        btn_first.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<User> item = new ArrayList<User>();
                User user = User.first(User.class);
                user.setName("ini sudah update....");
                user.save();

                user = User.first(User.class);
                item.add(user);

                adapter.notifyDataSetChanged();
            }
        });

        btn_change.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // delete all table record
                List<User> data = User.listAll(User.class);
                User.deleteAll(User.class);

                for (int i = 0; i < 10; i++) {
                    User user = new User();
                    user.setKode("kode-update" + i);
                    user.setName("Name-update-" + i);
                    user.save();
                }

                data = User.listAll(User.class);
                adapter.setmItems(data);
                adapter.notifyDataSetChanged();
            }
        });
    }

    private void generateRecord(){
        for (int i = 0; i < 10; i++) {
            User user = new User();
            user.setKode("kode" + i);
            user.setName("Name-" + i);
            user.save();
        }
    }

    private static class Adapter extends BaseAdapter{

        private Context context;
        private List<User> mItems;
        private Adapter(List<User> mItems, Context context){
            this.mItems = mItems;
            this.context= context;
        }

        @Override
        public int getCount() {
            return mItems.size();
        }

        @Override
        public Object getItem(int position) {
            return mItems.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            ViewHolder holder;
            LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            if(convertView == null){
                convertView = mInflater.inflate(R.layout.list_item, null);

                holder = new ViewHolder();

                holder.tv_kode = (TextView) convertView.findViewById(R.id.tv_kode);
                holder.tv_nama = (TextView) convertView.findViewById(R.id.tv_name);
                convertView.setTag(holder);
            }else{
                holder = (ViewHolder) convertView.getTag();
            }

            holder.tv_kode.setText(mItems.get(position).getKode());
            holder.tv_nama.setText(mItems.get(position).getName());

            return convertView;
        }

        static class ViewHolder{
            TextView tv_kode;
            TextView tv_nama;
        }

        public void setmItems(List<User> mItems) {
            this.mItems = mItems;
        }
    }
}
